import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccountCustomerService } from '../account-customer.service';
import { openAccount } from '../models/openAccountVO';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  customerID: number | undefined;
  amount: number | undefined;

  constructor(private AccountCustomerService: AccountCustomerService, private _snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
  }

  async openCurrentAccount() {

    (await this.AccountCustomerService.openAccount(this.customerID, this.amount)).subscribe(data => {
      let openAcc: openAccount = data;
      console.log(openAcc);
      this._snackBar.open("Account ID: " + openAcc.accountId + " with Balance: " + openAcc.balance + " was successfully Opened for CustomerID: " + openAcc.customerID, "OK", { duration: 5000, panelClass: ['success-snackbar'] });
      this.customerID=undefined;
      this.amount=undefined;

    },
      error => {
        this._snackBar.open(error.error.message, "Ok", { duration: 2000, panelClass: ['error-snackbar'] });
      });

  }


}
