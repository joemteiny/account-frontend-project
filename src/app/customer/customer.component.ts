import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AccountCustomerService } from '../account-customer.service';
import { AccountTransaction } from '../models/accountTransactionVO';
import { customerInfo } from '../models/customerInfoVO';
import { transaction } from '../models/transactionVO';



@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customerID: number | undefined;
  customerData: any
  displayedColumns: string[] = ['id'];
  name: any;
  surname: any;
  id: any;
  accountTran: any;
  accTrans: Array<any>=[];
  tran:transaction[] =[];


  constructor(private AccountCustomerService: AccountCustomerService, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }


  async getCustomerInfo() {

    (await this.AccountCustomerService.getCustomerInfo(this.customerID)).subscribe(data => {

      let cusinfo: customerInfo = data;
      let acctran:AccountTransaction[] = cusinfo.accountTrans;
     
      this.accountTran = cusinfo.accountTrans;

      this.id = cusinfo.id;
      this.surname = cusinfo.surname;
      this.name = cusinfo.name;

       for (let i=0;i<acctran.length;i++){ 
      this.tran= cusinfo.accountTrans[i].transactions;
      this.accTrans.push(cusinfo.accountTrans[i].transactions[0]);

          
       }
       console.log(this.accTrans);
      
    }, error => {
      this._snackBar.open(error.error.message, "Ok", { duration: 2000, panelClass: ['error-snackbar'] });
    });



  }
}
