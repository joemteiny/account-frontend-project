import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { customerInfo } from './models/customerInfoVO';
import { openAccount } from './models/openAccountVO';

@Injectable({
  providedIn: 'root'
})
export class AccountCustomerService {

  constructor(private http: HttpClient) { }



  async openAccount(customerID: any, initialCredit: any): Promise<Observable<openAccount>> {
    let url: string = "http://localhost:8080/account/openAccount";

    var openAcc = { "customerID": customerID, "initialCredit": initialCredit };

    return this.http.post<void>(url, openAcc).pipe(map((responseData: any) => {

      return responseData;

    }));
  }


  async getCustomerInfo(customerId: any): Promise<Observable<customerInfo>> {
    let url: string = "http://localhost:8080/customer/customerInfo";
    let params = new HttpParams().set("customerId", customerId.toString());

    return this.http.get<customerInfo>(url, { params: params}).pipe(map(responseData => {
        return responseData;
    }));
}

}
